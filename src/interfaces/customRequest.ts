import { Request } from 'express';
import { UserI } from './user';

export interface CustomRequest extends Request {
  user?: UserI
}
