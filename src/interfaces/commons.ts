export interface Phone {
  id?: string
  type: string
  value: string
}

export interface Address {
  id?: string
  type: string
  value: string
}

export interface Action {
  id: string
  action: string
  description?: string
}
