import {
  CodeOfTax, MethodOfPayment, SriStatus, TypeOfIdentificationBuyer,
} from '../enums/sriEnums';

export interface NewBillI {
  id?: string;
  name: string;
  identification: string;
  address: string;
  date: Date;
  subTotalIva: number;
  subTotalIce: number;
  subTotalZero: number;
  totalDiscount: number
  iva: number;
  ice?: number;
  total: number;
  createdBy: string;
  updatedBy: string;
  createdAt?: Date;
  updatedAt?: Date;
  companyId: string;
  billNumber: string;
  isActive: boolean;
  accessKey: string
  fileName?: string
  status?: SriStatus
  details: Detail[];
  branchOffice: string;
  boxCode: string;
  number: string;
  branchOfficeAddress: string;
  identificationType: TypeOfIdentificationBuyer;
  methodOfPayment: MethodOfPayment;
  codeOfTax: CodeOfTax;
}

export interface Detail {
  id?: string;
  code: string;
  amount: number;
  detail: string;
  unitPrice: number;
  tax: number;
  discount: number;
  total: number;
}

export interface TotalBill {
  subTotalTax: number
  subTotalZero: number
  totaltax: number
  totalDiscount: number
  details: Detail[]
}
