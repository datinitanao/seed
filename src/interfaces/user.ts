import { CommonEnum } from '../enums/common';
import { Address, Phone } from './commons';

export interface NewUserI {
  id?: string
  name: string
  lastName: string
  email: string
  password: string
  dni: string
  owner: boolean
  createdBy: string
  isActive: boolean
  phones: Phone []
  addresses: Address []
  resources: string []
  createdAt?: Date
  updatedAt?: Date
}

export type UserI = Omit<NewUserI, 'password'>;

export interface CommonI {
  id?: string
  type: CommonEnum
  value: string
  description?: string
}

export interface CredentialI {
  token: string,
  user: {
    name: string
  }
}

export interface UserDataI {
  id: string,
  roles: string []
}
