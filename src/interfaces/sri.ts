import {
  CodeOfTax,
  DocumentName,
  Environment,
  Flag,
  MethodOfPayment,
  TypeOfEmition,
  TypeOfIdentificationBuyer,
  TypeOfTaxs,
} from '../enums/sriEnums';

export interface SriBillSales {
  factura: Factura;
}

export interface Factura {
  infoTributaria: InfoTributaria;
  infoFactura: InfoFactura;
  detalles: Detalles;
  _id: string;
  _version: string;
}

export interface Detalles {
  detalle: Detalle[];
}

export interface Detalle {
  codigoPrincipal: string;
  codigoAuxiliar?: string;
  descripcion: string;
  cantidad: number;
  precioUnitario: number;
  descuento: number;
  precioTotalSinImpuesto: number;
  detallesAdicionales?: DetallesAdicionales;
  impuestos: Impuestos;
}

export interface DetallesAdicionales {
  detAdicional: DetAdicional;
}

export interface DetAdicional {
  _nombre: string;
  _valor: string;
}

export interface Impuestos {
  impuesto: Impuesto[];
}

export interface Impuesto {
  codigo: TypeOfTaxs;
  codigoPorcentaje?: CodeOfTax;
  tarifa?: number;
  baseImponible: number;
  valor: number;
}

export interface InfoFactura {
  fechaEmision: string;
  dirEstablecimiento: string;
  obligadoContabilidad?: Flag;
  tipoIdentificacionComprador: TypeOfIdentificationBuyer;
  razonSocialComprador: string;
  identificacionComprador: string;
  totalSinImpuestos: number;
  totalDescuento: number;
  totalConImpuestos: TotalConImpuestos;
  propina: number;
  importeTotal: number;
  moneda: string;
  pagos: Pagos;
}

export interface Pagos {
  pago: Pago;
}

export interface Pago {
  formaPago: MethodOfPayment;
  total: number;
  plazo?: number;
  unidadTiempo?: number;
}

export interface TotalConImpuestos {
  totalImpuesto: Impuesto[];
}

export interface InfoTributaria {
  ambiente: Environment;
  tipoEmision: TypeOfEmition;
  razonSocial: string;
  nombreComercial?: string;
  ruc: string;
  claveAcceso: string;
  codDoc: DocumentName;
  estab: string;
  ptoEmi: string;
  secuencial: string;
  dirMatriz: string;
}
