import { Request, Response } from 'express';
import httpStatus from 'http-status-codes';
import { UserSchema } from '../schemas/userSchema';
import validator from '../utils/validate';

import UserService from '../services/user';
import Errors from '../middlewares/errors';
import InvoiceError from '../utils/invoiceError';
import { errorType } from '../utils/erroTypes';
import { IdSchema } from '../schemas/idSchema';
import ResourcesService from '../services/resources';
import { CustomRequest } from '../interfaces/customRequest';

export const createUser = async (
  req: Request,
  res: Response,
): Promise<void> => {
  try {
    const { body } = req;
    await validator(UserSchema, body);
    await UserService.createUser(body);
    res.status(httpStatus.CREATED).send('User Created');
  } catch (error) {
    Errors.handleErrors(error, res);
  }
};

export const getAllUsers = async (
  req: Request,
  res: Response,
): Promise<void> => {
  try {
    const users = await UserService.getAllUsers();
    res.status(httpStatus.OK).send(users);
  } catch (error) {
    Errors.handleErrors(error, res);
  }
};

export const getUserById = async (
  req: Request,
  res: Response,
): Promise<void> => {
  try {
    const {
      params: { id },
    } = req;
    const user = await UserService.getUserById(id);
    if (user === null) {
      throw new InvoiceError(errorType.notFound, 'User not found');
    }
    res.status(httpStatus.OK).send(user);
  } catch (error) {
    Errors.handleErrors(error, res);
  }
};

export const updateUser = async (
  req: Request,
  res: Response,
): Promise<void> => {
  try {
    const { params, body } = req;
    await validator(IdSchema, params);
    await validator(UserSchema, body);
    await UserService.updateUserById(body, params.id);
    res.status(httpStatus.OK).send('User Updated');
  } catch (error) {
    Errors.handleErrors(error, res);
  }
};

export const getAccess = async (
  req: CustomRequest,
  res: Response,
) => {
  const { user } = req;
  const resources = await ResourcesService.getAllResourses();
  let access;
  if (user?.owner) {
    access = await UserService.getAllAccess(resources);
  } else {
    access = await UserService.getAccess(resources, user);
  }

  res.status(httpStatus.OK).send(access);
};

export default {
  createUser,
  getAllUsers,
  getUserById,
  updateUser,
  getAccess,
};
