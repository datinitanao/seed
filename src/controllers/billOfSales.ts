// import { NextFunction, Request, Response } from 'express';
// import httpStatus from 'http-status-codes';
// import { NewBillI } from '../interfaces/bill';
// import { BillSchema } from '../schemas/billSchema';

// import * as billOfSalesService from '../services/billOfSales';
// import { getClientByDni } from '../services/client';
// import { errorType } from '../utils/erroTypes';
// import InvoiceError from '../utils/invoiceError';
// import { createBillOfSalesDetails } from '../utils/utils';

// import validator from '../utils/validate';
// import { accessKeyGenerator, getDateDDMMAAAA } from '../utils/sriUtils';
// import { DocumentName, Environment, TypeOfEmition } from '../enums/sriEnums';
// import { findCompanyById } from '../services/company';
// import { findBoxById, increaseBoxBillNumber } from '../services/box';
// import { createBillXml } from '../services/signBill';

// export const createBillOfSales = async (
//   req: Request,
//   res: Response,
//   next: NextFunction,
// ): Promise<void> => {
//   try {
//     const { body } = req;
//     await validator(BillSchema, body);

//     const client = await getClientByDni(body.dni);
//     const company = await findCompanyById(body.companyId);
//     const branchOffice = company.branchOffices.find((branch) => branch.id === body.branchOfficeId);
//     if (!branchOffice) {
//       throw new InvoiceError(errorType.notFound, 'branchOffice not found', null);
//     }
//     const box = await findBoxById(body.boxId);

//     const {
//       subTotalTax,
//       subTotalZero,
//       totalDiscount,
//       totaltax,
//       details,
//     } = await createBillOfSalesDetails(body.details, company.taxValue.value);

//     const branchOfficeCode = branchOffice.code.padStart(3, '0');
//     const boxCode = box.box.toString().padStart(3, '0');
//     const billNumber = box.billNumber.toString().padStart(9, '0');
//     const number = `${branchOfficeCode}-${boxCode}-${billNumber}`;
//     const accessKey = accessKeyGenerator(
//       getDateDDMMAAAA(body.date),
//       DocumentName.FACTURA,
//       company.ruc,
//       Environment.PRUEBAS,
//       branchOfficeCode,
//       boxCode,
//       billNumber,
//       '12345678',
//       TypeOfEmition.EMISION_NORMAL,
//     );

//     const newBill: NewBillI = {
//       name: `${client.name} ${client.lastName}`,
//       identification: client.dni,
//       address: client.address,
//       date: body.date,
//       subTotalIva: subTotalTax,
//       subTotalIce: 0,
//       subTotalZero,
//       totalDiscount,
//       iva: totaltax,
//       ice: 0,
//       total: subTotalTax + subTotalZero + totaltax - totalDiscount,
//       accessKey,
//       details,
//       createdBy: body.createdBy,
//       updatedBy: body.updatedBy,
//       isActive: true,
//       companyId: body.companyId,
//       number,
//       fileName: `${client.name.replace(/\s/g, '')}-${client.lastName.replace(/\s/g, '')}-${number}`,
//       codeOfTax: company.taxValue.codeOfTax,
//       branchOffice: branchOffice.code.toString().padStart(3, '0'),
//       branchOfficeAddress: branchOffice.address,
//       identificationType: body.identificationType,
//       methodOfPayment: body.methodOfPayment,
//       billNumber,
//       boxCode,
//     };

//     await billOfSalesService.createBillOfSales(newBill);
//     await increaseBoxBillNumber(box.id, box.billNumber);
//     createBillXml(accessKey);
//     res.status(httpStatus.CREATED).send('Bill Created');
//   } catch (error) {
//     next(error);
//   }
// };

// export const getAllBillOfSales = async (
//   req: Request,
//   res: Response,
//   next: NextFunction,
// ): Promise<void> => {
//   try {
//     const bills = await billOfSalesService.getAllBillOfSales();
//     res.status(httpStatus.CREATED).send(bills);
//   } catch (error) {
//     next(error);
//   }
// };
