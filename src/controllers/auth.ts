import { Request, Response } from 'express';
import httpStatus from 'http-status-codes';
// import bwipjs from 'bwip-js';

import { CredentialsSchema } from '../schemas/credentialsSchema';
import { checkCredential } from '../services/auth';
// import { getBillOfSalesByAccesskey } from '../services/billOfSales';
// import { findCompanyById } from '../services/company';
import validator from '../utils/validate';
import Errors from '../middlewares/errors';

export const login = async (req: Request, res: Response): Promise<void> => {
  try {
    const { body } = req;
    await validator(CredentialsSchema, body);
    const { username: email, password } = body;
    const user = await checkCredential(email, password);
    res.status(httpStatus.OK).json({ user });
  } catch (err) {
    Errors.handleErrors(err, res);
  }
};

// export const vaidateToken = async (
//   req: Request,
//   res: Response,
//   next: NextFunction,
// ): Promise<void> => {
//   try {
//     const {
//       query,
//       headers: { token },
//     } = req;
//     await decodeToken(token);
//     console.log(`called from ${query.path}`);
//     res.status(httpStatus.OK).json({ message: 'token is valid' });
//   } catch (err) {
//     next(err);
//   }
// };

// export const views = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
//   try {
//     const bill = await getBillOfSalesByAccesskey('');
//     const company = await findCompanyById(bill.companyId);
//     const img = await bwipjs.toBuffer({
//       bcid: 'code128', // Barcode type
//       text: bill.accessKey, // Text to encode
//       scale: 2, // 3x scaling factor
//       height: 10, // Bar height, in millimeters
//       includetext: true, // Show human-readable text
//       textxalign: 'center', // Always good to set this
//     });

//     const imgBase64 = `data:image/jpg;base64,${Buffer.from(img).toString('base64')}`;

//     res.render('../views/ride/index', { bill, company, imgBase64 });
//   } catch (err) {
//     next(err);
//   }
// };
export default {
  login,
};
