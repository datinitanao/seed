import { Request, Response } from 'express';

import httpStatus from 'http-status-codes';
import Errors from '../middlewares/errors';

import ResourseSerive from '../services/resources';

const getAllResource = async (
  req: Request,
  res: Response,
): Promise<void> => {
  try {
    const resource = await ResourseSerive.getAllResourses();
    res.status(httpStatus.OK).send(resource);
  } catch (error: any) {
    Errors.handleErrors(error, res);
  }
};

export default {
  getAllResource,
};
