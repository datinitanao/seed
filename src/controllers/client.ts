import { NextFunction, Request, Response } from 'express';
import httpStatus from 'http-status-codes';
import { IdSchema } from '../schemas/idSchema';

import ClientService from '../services/client';

import validator from '../utils/validate';
import { ClientSchema } from '../schemas/clientSchema';
import Errors from '../middlewares/errors';
import InvoiceError from '../utils/invoiceError';
import { errorType } from '../utils/erroTypes';

import ResourseService from '../services/resources';

const createClient = async (
  req: Request,
  res: Response,
): Promise<void> => {
  try {
    const { body } = req;
    await validator(ClientSchema, body);
    await ClientService.createClient(body);
    res.status(httpStatus.CREATED).send('client Created');
  } catch (error) {
    Errors.handleErrors(error, res);
  }
};

const getAllClients = async (
  req: Request,
  res: Response,
): Promise<void> => {
  try {
    await ResourseService.getAllResourses();
    const clients = await ClientService.getAllClients();
    if (clients.length <= 0) {
      throw new InvoiceError(errorType.notFound, 'Clients not found');
    }
    res.status(httpStatus.OK).send(clients);
  } catch (error) {
    Errors.handleErrors(error, res);
  }
};

export const getClientById = async (
  req: Request,
  res: Response,
): Promise<void> => {
  try {
    const {
      params: { id },
    } = req;

    const client = await ClientService.getClientById(id);
    if (client === null) {
      throw new InvoiceError(errorType.notFound, 'Client not found');
    }
    res.status(httpStatus.OK).send(client);
  } catch (error) {
    Errors.handleErrors(error, res);
  }
};

const updateClientById = async (
  req: Request,
  res: Response,
): Promise<void> => {
  try {
    const { params, body } = req;
    await validator(IdSchema, params);
    await validator(ClientSchema, body);
    await ClientService.updateClientById(body, params.id);
    res.status(httpStatus.NO_CONTENT).send('client Updated');
  } catch (error) {
    Errors.handleErrors(error, res);
  }
};

export const deleteClientById = async (
  req: Request,
  res: Response,
  next: NextFunction,
): Promise<void> => {
  try {
    const {
      params,
      body: { updatedBy },
    } = req;
    await validator(IdSchema, params);
    await ClientService.deactivateClientById(params.id, updatedBy);
    res.status(httpStatus.NO_CONTENT).send('client deativated');
  } catch (error) {
    next(error);
  }
};

const clientController = {
  createClient,
  getAllClients,
  getClientById,
  updateClientById,
  deleteClientById,
};

export default clientController;

// export const getClientByDni = async (
//   req: Request,
//   res: Response,
//   next: NextFunction,
// ): Promise<void> => {
//   try {
//     const {
//       params: { dni },
//     } = req;
//     const clients = await clientService.getClientByDni(dni);
//     res.status(httpStatus.OK).send(clients);
//   } catch (error) {
//     next(error);
//   }
// };
