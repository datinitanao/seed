// import { NextFunction, Request, Response } from 'express';
// import fs from 'fs';
// import path from 'path';

// const uploadImages = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
//   try {
//     let image = '';
//     if (req.body.image === 'empty.jpg' || req.body.image === '') {
//       image = `${new Date().getTime()}.jpg`;
//     } else {
//       image = req.body.image;
//     }

//     const imgPath = path.join(__dirname, '../../public/images', `${image}`);
//     const m = req.body.img.match(/^data:([A-Za-z-+/]+);base64,(.+)$/);
//     const b = Buffer.from(m[2], 'base64');

//     await fs.writeFileSync(imgPath, b, 'binary');

//     res.status(200).json({ message: 'image upload', image });
//   } catch (err) {
//     next(err);
//   }
// };

// export default uploadImages;
