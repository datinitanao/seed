import express, { Express } from 'express';

import bodyParser from 'body-parser';
import morgan from 'morgan';
import path from 'path';
import Errors from './middlewares/errors';
import router from './router';

import connectDB from './db/connect';

const app: Express = express();

app.use(bodyParser.urlencoded({ limit: '50mb', extended: false }));
app.use(bodyParser.json({ limit: '50mb' }));

const port = process.env.PORT ?? 3000;

const css = path.join(__dirname, '../public');

app.use(express.static(css));
app.use(morgan('dev'));
app.use('/api', router);
app.use(Errors.handleErrors);

app.listen(port, async () => {
  await connectDB();
  console.log(`[server]: Server is running at http://localhost:${port}`);
});
