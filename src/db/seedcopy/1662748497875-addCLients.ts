import { MigrationInterface, QueryRunner } from 'typeorm';

export class addCLients1662748497875 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `INSERT INTO
            clients(name, "lastName", email, dni, "phone", "address", "createdBy" , "updatedBy", "isActive", "companyId") VALUES ('PRUEBAS', 'S SERVICIO DE RENTAS 
            INTERNAS', 'client1@gmail.com','1713328506001', '07271884', 'address 1', 'system', 'system', 'true', '667ef404-2896-4630-9760-baf3de51e04e')`
    );

    await queryRunner.query(
        `INSERT INTO
              clients(name, "lastName", email, dni, "phone", "address", "createdBy" , "updatedBy", "isActive", "companyId")
              VALUES(
                  'client2',
                  'lastClient2',
                  'client2@gmail.com',
                  '12344452',
                  '07271885',
                  'address 2',
                  'system',
                  'system',
                  'true',
                  '667ef404-2896-4630-9760-baf3de51e04e'
              )`
      );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {}
}
