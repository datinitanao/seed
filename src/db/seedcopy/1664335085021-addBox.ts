import { MigrationInterface, QueryRunner } from 'typeorm';

export class addBox1664335085021 implements MigrationInterface {

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `INSERT INTO
      "Boxes" (id, box, "billNumber", "companyId", "branchOfficeId")
                  VALUES(
                      '03fe6972-5fb3-4af5-af33-7b3c913d678a',
                      '1',
                      '1',
                      '667ef404-2896-4630-9760-baf3de51e04e',
                      '277a9abe-75fe-44e8-be1f-ba567fd49975'
                    )`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
  }

}
