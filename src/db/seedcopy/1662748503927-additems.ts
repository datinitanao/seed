import { MigrationInterface, QueryRunner } from 'typeorm';

export class additems1662748503927 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `INSERT INTO
                  items(id, name, code, "barCode", "hasIva", "purchasePrice", stock,  "pvps", "type", "companyId")
                  VALUES(
                      'cc196daa-306f-11ed-a261-0242ac120002',
                      'papelera',
                      'I001',
                      'IBC001',
                       true,
                      2.25,
                      0,
                      '[{"id":1, "value": 6.25, "percent": 10}]',
                      'PRODUCT',
                      '667ef404-2896-4630-9760-baf3de51e04e')`
          );
          await queryRunner.query(
            `INSERT INTO
                  items(id, name, code, "barCode", "hasIva", "purchasePrice", stock,  "pvps",  "type", "companyId")
                  VALUES(
                      '169ee313-384a-4584-ba65-251f4e534ed9',
                      'armador king junnior',
                      'I002',
                      'IBC002',
                       true,
                      2.50,
                      0,
                      '[{"id":1, "value": 2.05, "percent": 10}, {"id":2, "value": 2.70, "percent": 10}]',
                      'PRODUCT',
                      '667ef404-2896-4630-9760-baf3de51e04e')`
          );
          await queryRunner.query(
            `INSERT INTO
                  items(id, name, code, "barCode", "hasIva", "purchasePrice", stock,  "pvps",  "type", "companyId")
                  VALUES(
                      'ce2d68d2-a431-4a37-a3c7-6d4805dc862b',
                      'termo big home acero',
                      'I003',
                      'IBC003',
                       true,
                      0.10,
                      0,
                      '[{"id":1, "value": 5.80, "percent": 10}]',
                      'PRODUCT',
                      '667ef404-2896-4630-9760-baf3de51e04e')`
          );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
