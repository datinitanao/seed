import { MigrationInterface, QueryRunner } from "typeorm"
import bcrypt from 'bcrypt';

export class addUser1674912272786 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        const password =  bcrypt.hashSync('test1', 10);
        await queryRunner.query(
            `INSERT INTO users(name, "lastName", email, password, dni, "createdBy" , "updatedBy", "isActive", owner) VALUES ('test1', 'tini', 'test1@gmail.com', '${password}', '12344432', 'system', 'system', true, true)`
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {}
}
