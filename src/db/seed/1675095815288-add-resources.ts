import { randomUUID } from "crypto"
import { MigrationInterface, QueryRunner } from "typeorm"

export class addResources1675095815288 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        const resources = [
            {
                value: 'user',
                name: 'Usuario',
            },
            {
                value: 'client',
                name: 'Cliente',
            },
            {
                value: 'resources',
                name: 'Recursos',
            }
        ]

        const actions = () => {
            return `[
                {
                    "id": "${randomUUID()}",
                    "action": "post",
                    "description": "Crear"
                },
                {
                    "id": "${randomUUID()}",
                    "action": "get",
                    "description": "Leer"
                },
                {
                    "id": "${randomUUID()}",
                    "action": "put",
                    "description": "Actualizar"
                },
                {
                    "id": "${randomUUID()}",
                    "action": "delete",
                    "description": "Eliminar"
                }
            ]`
        }
        const all = resources.map(async (resourse) => {    
           return await queryRunner.query(`INSERT INTO resources ("resource", "description", "actions") VALUES ('${resourse.value}', '${resourse.name}', '${actions()}')`);
        })

        await Promise.all(all)
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
