import { DataSource } from 'typeorm';
import config from './config';

const AppDataSource = new DataSource({
  ...config,
  migrations: [`${__dirname}/seed/**/*{.ts,.js}`],
});

export default AppDataSource;
