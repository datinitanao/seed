import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { Action } from '../../interfaces/commons';

@Entity('resources')
export default class ResourceEntity {
  @PrimaryGeneratedColumn('uuid')
    id!: string;

  @Column()
    resource!: string;

  @Column()
    description!: string;

  @Column('jsonb', { nullable: true })
    actions!: Action[];
}
