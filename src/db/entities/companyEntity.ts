// import {
//   Column,
//   Entity,
//   JoinColumn,
//   OneToMany,
//   OneToOne,
//   PrimaryGeneratedColumn,
// } from 'typeorm';
// // eslint-disable-next-line import/no-cycle
// import BranchOfficeEntity from './branchOfficeEntity';
// // eslint-disable-next-line import/no-cycle
// import TaxEntity from './taxEntity';

// @Entity('companies')
// export default class CompanyEntity {
//   @PrimaryGeneratedColumn('uuid')
//     id!: string;

//   @Column()
//     name!: string;

//   @Column()
//     companyName!: string;

//   @Column({ unique: true })
//     ruc!: string;

//   @Column()
//     mainAddress!: string;

//   @Column({ default: false })
//     hasAccounting!: boolean;

//   @Column({ default: 'DOLAR' })
//     coin!: string;

//   @OneToOne(() => TaxEntity, (tax) => tax.company)
//   @JoinColumn()
//     taxValue!: TaxEntity;

//   @OneToMany(
//     () => BranchOfficeEntity,
//     (branchOffice: BranchOfficeEntity) => branchOffice.company,
//     { eager: true, cascade: true },
//   )
//     branchOffices!: BranchOfficeEntity[];
// }
