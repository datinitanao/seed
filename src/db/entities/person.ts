import {
  PrimaryGeneratedColumn, Column,
} from 'typeorm';

export default class PersonEntity {
  @PrimaryGeneratedColumn('uuid')
    id!: string;

  @Column()
    name!: string;

  @Column()
    lastName!: string;

  @Column({ unique: true })
    email!: string;

  @Column({ unique: true })
    dni!: string;

  @Column({ nullable: true })
    createdBy!: string;

  @Column({ nullable: true })
    updatedBy!: string;

  @Column({ default: true })
    isActive!: boolean;
}
