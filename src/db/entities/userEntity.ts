import { Entity, Column } from 'typeorm';
import { Address, Phone } from '../../interfaces/commons';
import PersonEntity from './person';

@Entity('users')
export default class UserEntity extends PersonEntity {
  @Column({ select: false })
    password!: string;

  @Column({ default: false })
    owner!: boolean;

  @Column('jsonb', { nullable: true, default: [] })
    resources!: string[];

  @Column('jsonb', { nullable: true })
    phones!: Phone[];

  @Column('jsonb', { nullable: true })
    addresses!: Address[];
}
