import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('roles')
export default class RoleEntity {
  @PrimaryGeneratedColumn('uuid')
    id!: string;

  @Column()
    name!: string;

  @Column({ nullable: true })
    description?: string;

  @Column('jsonb', { nullable: true })
    actions!: string[];
}
