// import {
//   Column,
//   Entity,
//   JoinTable,
//   ManyToMany,
//   PrimaryGeneratedColumn,
// } from 'typeorm';
// import { ItemEnum } from '../../enums/ItemType';
// import { Pvp } from '../../interfaces/items';
// // eslint-disable-next-line import/no-cycle
// import CategoryEntity from './categoryEntity';

// @Entity('items')
// export default class ItemEntity {
//   @PrimaryGeneratedColumn('uuid')
//     id!: string;

//   @Column()
//     name!: string;

//   @Column({ nullable: true })
//     description?: string;

//   @Column({ unique: true, nullable: true })
//     code!: string;

//   @Column({ unique: true, nullable: true })
//     barCode!: string;

//   @Column({ default: false })
//     hasIva!: boolean;

//   @Column({ default: false })
//     hasIce!: boolean;

//   @Column({
//     type: 'decimal', precision: 20, scale: 5, default: 0.00,
//   })
//     purchasePrice!: number;

//   @Column({ type: 'enum', enum: ItemEnum })
//     type!: ItemEnum;

//   @Column({
//     type: 'decimal', precision: 20, scale: 5, default: 0.00,
//   })
//     stock!: number;

//   @Column('jsonb')
//     pvps!: Pvp[];

//   @Column({ default: true })
//     isActive!: boolean;

//   @Column({ nullable: true })
//     photo!: string;

//   @ManyToMany(() => CategoryEntity, (category) => category.items)
//   @JoinTable()
//     categories!: CategoryEntity[];

//   @Column()
//     companyId!: string;
// }
