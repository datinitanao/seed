// import {
//   Column, CreateDateColumn, Entity, ManyToMany, PrimaryGeneratedColumn, UpdateDateColumn,
// } from 'typeorm';
// // eslint-disable-next-line import/no-cycle
// import ItemEntity from './itemEntity';

// @Entity('categories')
// export default class CategoryEntity {
//   @PrimaryGeneratedColumn('uuid')
//     id!: string;

//   @Column()
//     name!: string;

//   @Column({ nullable: true })
//     description?: string;

//   @Column()
//     createdBy!: string;

//   @Column({ nullable: true })
//     updatedBy!: string;

//   @CreateDateColumn({ type: 'timestamptz', name: 'created_at' })
//     createdAt!: Date;

//   @UpdateDateColumn({ type: 'timestamptz', name: 'updated_at' })
//     updatedAt!: Date;

//   @ManyToMany(() => ItemEntity, (item) => item.categories, { cascade: true })
//     items!: ItemEntity[];

//   @Column()
//     companyId!: string;
// }
