// import {
//   Column, CreateDateColumn, PrimaryGeneratedColumn, UpdateDateColumn,
// } from 'typeorm';

// export default class BillEntity {
//   @PrimaryGeneratedColumn('uuid')
//     id!: string;

//   @Column()
//     name!: string;

//   @Column()
//     identification!: string;

//   @Column()
//     address!: string;

//   @Column()
//     date!: Date;

//   @Column({
//     type: 'decimal', precision: 20, scale: 5, default: 0.00,
//   })
//     subTotalIva!: number;

//   @Column({
//     type: 'decimal', precision: 20, scale: 5, default: 0.00,
//   })
//     subTotalIce!: number;

//   @Column({
//     type: 'decimal', precision: 20, scale: 5, default: 0.00,
//   })
//     subTotalZero!: number;

//   @Column({
//     type: 'decimal', precision: 20, scale: 5, default: 0.00,
//   })
//     totalDiscount!: number;

//   @Column({
//     type: 'decimal', precision: 20, scale: 5, default: 0.00,
//   })
//     iva!: number;

//   @Column({
//     type: 'decimal', precision: 20, scale: 5, default: 0.00,
//   })
//     ice!: number;

//   @Column({
//     type: 'decimal', precision: 20, scale: 5, default: 0.00,
//   })
//     total!: number;

//   @Column()
//     createdBy!: string;

//   @Column({ nullable: true })
//     updatedBy!: string;

//   @CreateDateColumn({ type: 'timestamptz', name: 'created_at' })
//     createdAt!: Date;

//   @UpdateDateColumn({ type: 'timestamptz', name: 'updated_at' })
//     updatedAt!: Date;

//   @Column()
//     companyId!: string;

//   @Column()
//     billNumber!: string;
// }
