// import { Column, Entity, OneToMany } from 'typeorm';
// import {
//   CodeOfTax, MethodOfPayment, SriStatus, TypeOfIdentificationBuyer,
// } from '../../enums/sriEnums';
// import BillEntity from './BillEntity';
// // eslint-disable-next-line import/no-cycle
// import BillOfSalesDetailEntity from './BillOfSaleDetailEntity';

// @Entity('bill_of_sales')
// export default class BillOfSalesEntity extends BillEntity {
//   @Column()
//     isActive!: boolean;

//   @Column({ unique: true })
//     accessKey!: string;

//   @Column({ nullable: true })
//     fileName!: string;

//   @Column({ default: SriStatus.NO_ENVIADA })
//     status!: SriStatus;

//   @Column()
//     branchOffice!: string;

//   @Column()
//     boxCode!: string;

//   @Column()
//     number!: string;

//   @Column()
//     branchOfficeAddress!: string;

//   @Column()
//     identificationType!: TypeOfIdentificationBuyer;

//   @Column()
//     methodOfPayment!: MethodOfPayment;

//   @Column()
//     codeOfTax!: CodeOfTax;

//   @OneToMany(
//     () => BillOfSalesDetailEntity,
//     (detail: BillOfSalesDetailEntity) => detail.bill,
//     { eager: true, cascade: true },
//   )
//     details!: BillOfSalesDetailEntity [];
// }
