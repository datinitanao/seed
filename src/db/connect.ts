import AppDataSource from './ormconfig';

const connectDB = async (): Promise<void> => {
  await AppDataSource.initialize();
  console.log('db Connected');
};

export default connectDB;
