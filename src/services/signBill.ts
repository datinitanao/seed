// import puppeteer from 'puppeteer';
// import fs from 'fs';
// import path from 'path';
// import {
//   CodeOfTax, DocumentName, Environment, Flag, SriStatus, TypeOfEmition, TypeOfTaxs,
// } from '../enums/sriEnums';
// import { Detalle, SriBillSales } from '../interfaces/sri';
// import { getDateFormat } from '../utils/sriUtils';
// import { getBillOfSalesByAccesskey, updateBillOfSalesByAccesskey } from './billOfSales';
// import { findCompanyById } from './company';
// import InvoiceError from '../utils/invoiceError';
// import { errorType } from '../utils/erroTypes';
// import { sleep } from '../utils/utils';
// import { NewBillI } from '../interfaces/bill';
// import CompanyEntity from '../db/entities/companyEntity';

// const { exec } = require('child_process');
// // eslint-disable-next-line @typescript-eslint/no-var-requires
// const X2JS = require('x2js');
// // eslint-disable-next-line @typescript-eslint/no-var-requires
// const { soap } = require('strong-soap');

// export const validateBillToSri = async (accessKey: string) => {
//   const url = 'https://celcer.sri.gob.ec/comprobantes-electronicos-ws/AutorizacionComprobantesOffline?wsdl';

//   const options = {};
//   soap.createClient(url, options, (err: any, client: any) => {
//     client.autorizacionComprobante(
//       { claveAccesoComprobante: accessKey },
//       async (error: any, result: any) => {
//         const bill = result.RespuestaAutorizacionComprobante.autorizaciones.autorizacion.at(0);
//         if (bill.estado !== SriStatus.AUTORIZADO) {
//           console.log('validateBillToSri', bill.message);
//         }
//         if (bill.estado !== SriStatus.NO_AUTORIZADO) {
//           console.log('validateBillToSri', bill.message);
//         }
//         console.log(JSON.stringify(result));
//         await updateBillOfSalesByAccesskey(accessKey, bill.estado);
//       },
//     );
//   });
// };

// export const sendBillToSri = async (filePath: string, accessKey: string) => {
//   //   https://celcer.sri.gob.ec/comprobantes-electronicos-ws/RecepcionComprobantesOffline?wsdl
//   // https://celcer.sri.gob.ec/comprobantes-electronicos-ws/AutorizacionComprobantesOffline?wsdl
//   const url = 'https://celcer.sri.gob.ec/comprobantes-electronicos-ws/RecepcionComprobantesOffline?wsdl';

//   const file = fs.readFileSync(filePath, 'utf8');
//   const xmlBase64 = Buffer.from(file).toString('base64');
//   const options = {};
//   soap.createClient(url, options, (err: any, client: any) => {
//     client.validarComprobante({ xml: xmlBase64 }, async (error: any, result: any) => {
//       if (error) {
//         throw new InvoiceError(errorType.sri, 'Bill not accepted');
//       }
//       const status = result.RespuestaRecepcionComprobante.estado;
//       if (status === SriStatus.RECIBIDA) {
//         updateBillOfSalesByAccesskey(accessKey, SriStatus.RECIBIDA);
//         await sleep(5000);
//         validateBillToSri(accessKey);
//       }
//     });
//   });
// };

// export const signBillXml = async (xmlPath: string, name: string, accessKey: string) => {
//   const JAR = path.join(__dirname, '../../', 'sriSignBill.jar');
//   const pathSignature = path.join(__dirname, '../../', 'diego_andres_tinitana_ortega.p12');
//   const passSignature = 'Estrellate12';
//   const pathOut = path.join(__dirname, '../../', 'firmados');
//   const nameFileOut = name;

//   const command = `java -jar ${JAR} ${xmlPath} ${pathSignature} ${passSignature} ${pathOut} ${nameFileOut}`;
//   exec(command, (err: any, result: any, stderr: any) => {
//     if (err) { console.log(err, '?????'); }
//     if (stderr) { console.log(stderr, '<<<<<'); }
//     sendBillToSri(path.join(__dirname, '../../', 'firmados', `${name}`), accessKey);
//   });
// };

// const createBillJson = async (bill: NewBillI, company: CompanyEntity) => {
//   const detail = bill.details.map((billDetail) => {
//     const newDetail: Detalle = {
//       codigoPrincipal: billDetail.code,
//       descripcion: billDetail.detail,
//       cantidad: billDetail.amount,
//       precioUnitario: billDetail.unitPrice,
//       descuento: billDetail.discount,
//       precioTotalSinImpuesto: billDetail.total,
//       impuestos: {
//         impuesto: [{
//           codigo: TypeOfTaxs.IVA,
//           codigoPorcentaje: billDetail.tax > 0 ? bill.codeOfTax : CodeOfTax.ZERO,
//           tarifa: billDetail.tax > 0 ? 12.0 : 0.00,
//           baseImponible: billDetail.total,
//           valor: billDetail.tax,
//         }],
//       },
//     };
//     return newDetail;
//   });

//   const billXml: SriBillSales = {
//     factura: {
//       _id: 'comprobante',
//       _version: '1.0.0',
//       infoTributaria: {
//         ambiente: Environment.PRUEBAS,
//         tipoEmision: TypeOfEmition.EMISION_NORMAL,
//         razonSocial: company.name,
//         ruc: company.ruc,
//         claveAcceso: bill.accessKey,
//         codDoc: DocumentName.FACTURA,
//         estab: bill.branchOffice,
//         ptoEmi: bill.boxCode,
//         secuencial: bill.billNumber,
//         dirMatriz: company.mainAddress,
//       },
//       infoFactura: {
//         fechaEmision: getDateFormat(bill.date),
//         dirEstablecimiento: bill.branchOfficeAddress,
//         obligadoContabilidad: company.hasAccounting ? Flag.SI : Flag.NO,
//         tipoIdentificacionComprador: bill.identificationType,
//         razonSocialComprador: bill.name,
//         identificacionComprador: bill.identification,
//         totalSinImpuestos: bill.subTotalIva,
//         totalDescuento: bill.totalDiscount,
//         totalConImpuestos: {
//           totalImpuesto: [
//             {
//               codigo: TypeOfTaxs.IVA,
//               codigoPorcentaje: bill.codeOfTax,
//               baseImponible: bill.subTotalIva,
//               valor: bill.iva,
//             },
//           ],
//         },
//         propina: 0.00,
//         importeTotal: bill.total,
//         moneda: company.coin,
//         pagos: {
//           pago: {
//             formaPago: bill.methodOfPayment,
//             total: bill.total,
//           },
//         },
//       },
//       detalles: {
//         detalle: detail,
//       },
//     },
//   };

//   return billXml;
// };

// export const createBillXml = async (accessKey: string) => {
//   const bill = await getBillOfSalesByAccesskey(accessKey);
//   const company = await findCompanyById(bill.companyId);

//   const billXml = await createBillJson(bill, company);
//   const filelname = bill.fileName || accessKey;

//   const x2js = new X2JS();

//   const xml = x2js.js2xml(billXml);
//   const xmlPath = path.join(__dirname, '../../nuevo', `${filelname}.xml`);

//   await fs.writeFileSync(xmlPath, xml, 'utf8');

//   signBillXml(xmlPath, `${filelname}`, accessKey);
// };

// export const createRideSri = async (accessKey: string) => {
//   const bill = await getBillOfSalesByAccesskey(accessKey);
//   if (bill && bill.status !== SriStatus.AUTORIZADO) {
//     throw new InvoiceError(errorType.sri, 'Bill not accepted');
//   }
//   // const company = await findCompanyById(bill.companyId);

//   // const billXml = await createBillJson(bill, company);

//   // const img = await bwipjs.toBuffer({
//   //   bcid: 'code128', // Barcode type
//   //   text: bill.accessKey, // Text to encode
//   //   scale: 2, // 3x scaling factor
//   //   height: 10, // Bar height, in millimeters
//   //   includetext: true, // Show human-readable text
//   //   textxalign: 'center', // Always good to set this
//   // });

//   // const imgBase64 = `data:image/jpg;base64,${Buffer.from(img).toString('base64')}`;

//   const navigator = await puppeteer.launch();
//   const page = await navigator.newPage();

//   await page.goto('http://localhost:8080/api/view');

//   const pdf = await page.pdf();

//   return pdf;
// };
