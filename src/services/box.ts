// import BoxEntity from '../db/entities/boxEntity';
// import AppDataSource from '../db/ormconfig';
// import { errorType } from '../utils/erroTypes';
// import InvoiceError from '../utils/invoiceError';

// export const findBoxById = async (id: string): Promise<BoxEntity> => {
//   const queryRunner = AppDataSource.getRepository(BoxEntity);
//   try {
//     const box = await queryRunner.findOne({ where: { id } });
//     if (!box) {
//       throw new InvoiceError(errorType.dataBase, 'Box not found');
//     }
//     return box;
//   } catch (err: any) {
//     console.log('BoxService: findBoxById>>', err.message);
//     throw new InvoiceError(errorType.dataBase, 'Box not found');
//   }
// };

// export const increaseBoxBillNumber = async (id: string, value: number): Promise<void> => {
//   const query = AppDataSource.getRepository(BoxEntity);
//   const billNumber = value + 1;
//   try {
//     const bill = await query
//       .createQueryBuilder()
//       .update(BoxEntity)
//       .set({ billNumber })
//       .where('id= :id', { id })
//       .execute();
//     if (bill.affected === 0) {
//       throw new InvoiceError(errorType.notFound, 'box not found');
//     }
//   } catch (err: any) {
//     console.log('BoxService: increaseBoxBillNumber>>', err.message);
//     throw new InvoiceError(errorType.dataBase, 'Box not found');
//   }
// };
