import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';

import dotenv from 'dotenv';

import UserService from './user';
import { CredentialI } from '../interfaces/user';
import InvoiceError from '../utils/invoiceError';
import { errorType } from '../utils/erroTypes';

dotenv.config();

export const checkCredential = async (email: string, password: string): Promise<CredentialI> => {
  const user = await UserService.getUserByEmail(email);

  if (user === null) {
    throw new InvoiceError(errorType.authentication, 'User no register');
  }

  if (!bcrypt.compareSync(password, user.password)) {
    throw new InvoiceError(errorType.authentication, 'Email or password are wrong');
  }
  const secret = process.env.JWT_KEY ?? '';

  return {
    token: jwt.sign({ id: user.id }, secret, { expiresIn: '1d' }),
    user: {
      name: user.name,
    },
  };
};

export const decodeToken = async (token: any): Promise<object> => {
  if (token === undefined || token === null) {
    throw new InvoiceError(errorType.authentication, 'Unauthorized!');
  }
  const checkToken = token.split(' ').length > 1
    ? token.split(' ')[1]
    : token;
  const secret = process.env.JWT_KEY ?? '';
  try {
    const tokenVerify: any = jwt.verify(checkToken, secret);
    const { id } = tokenVerify;
    return { userId: id };
  } catch (error) {
    throw new InvoiceError(errorType.Forbidden, 'jwt expired');
  }
};
