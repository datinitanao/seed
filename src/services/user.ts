import bcrypt from 'bcrypt';
import AppDataSource from '../db/ormconfig';

import UserEntity from '../db/entities/userEntity';
import { UserI, NewUserI } from '../interfaces/user';
import InvoiceError from '../utils/invoiceError';
// import { findRoles } from './role';
import { errorType } from '../utils/erroTypes';

import { getKeyDbError, passwordCreation } from '../utils/utils';
import ResourceEntity from '../db/entities/resourcesEntity';

export const createUser = async (user: NewUserI): Promise<void> => {
  const queryRunner = AppDataSource.getRepository(UserEntity);
  try {
    const userCreated = await queryRunner.save(
      {
        ...user,
        password: bcrypt.hashSync(passwordCreation(user), 10),
      },
    );
    console.log(
      `user_service: createUser, usercreated with id: ${userCreated.id}`,
    );
  } catch (err:any) {
    if (err.code === '23505') {
      throw new InvoiceError(errorType.dbUnique, getKeyDbError(err.detail));
    }
    throw new InvoiceError(errorType.dataBase, '', err);
  }
};

export const getAllUsers = async (): Promise<UserI[]> => {
  try {
    const query = AppDataSource.getRepository(UserEntity);
    return await query.find();
  } catch (err:any) {
    throw new InvoiceError(errorType.dataBase, '', err);
  }
};

const getUserByEmail = async (
  email: string,
): Promise<UserEntity | null> => {
  const query = AppDataSource.getRepository(UserEntity);
  try {
    return await query
      .createQueryBuilder('user')
      .where('user.email = :email', { email })
      .addSelect('user.password')
      .getOne();
  } catch (err: any) {
    throw new InvoiceError(errorType.dataBase, '', err);
  }
};

export const getUserById = async (id: string): Promise<UserI | null> => {
  try {
    const query = AppDataSource.getRepository(UserEntity);
    const clientEntitie = await query.findOne({
      where: { id },
    });
    return clientEntitie;
  } catch (err:any) {
    throw new InvoiceError(errorType.dataBase, '', err);
  }
};

export const updateUserById = async (
  user: UserI,
  id: string,
): Promise<void> => {
  try {
    const query = AppDataSource.getRepository(UserEntity);
    const userUpdated = await query
      .createQueryBuilder()
      .update(UserEntity)
      .set(user)
      .where('id= :id', { id })
      .execute();
    if (userUpdated.affected === 0) {
      throw new InvoiceError(errorType.notFound, 'User not found');
    }
    console.log(`user_service: updateUserById,  userUpdated with id: ${id}`);
  } catch (err:any) {
    console.log('UserService: updateUserById>>', err.message);
    throw new InvoiceError(errorType.dataBase, 'User not updated');
  }
};

export const getAccess = async (
  resources: ResourceEntity[],
  user?: UserI,
): Promise<void> => {
  const routes = resources.reduce((init: any, resource) => {
    const act = resource.actions.filter((action) => user?.resources.includes(action.id));
    if (act.length > 0) {
      const newRoute = {
        route: resource.resource,
        name: resource.description,
        routers: act,
      };
      return [...init, newRoute];
    }
    return init;
  }, []);
  return routes;
};

export const getAllAccess = async (
  resources: ResourceEntity[],
): Promise<any> => {
  const routes = resources.map((resource) => {
    const newRoute = {
      route: resource.resource,
      name: resource.description,
      routers: resource.actions,
    };
    return newRoute;
  });
  return routes;
};

export default {
  createUser,
  getAllUsers,
  getUserByEmail,
  getUserById,
  updateUserById,
  getAccess,
  getAllAccess,
};
