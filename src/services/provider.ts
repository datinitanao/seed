// import AppDataSource from '../db/ormconfig';

// import InvoiceError from '../utils/invoiceError';
// import { errorType } from '../utils/erroTypes';
// import ProviderEntity from '../db/entities/providerEntity';
// import { NewProviderI } from '../interfaces/provider';

// export const createProvider = async (provider: NewProviderI): Promise<void> => {
//   const queryRunner = AppDataSource.getRepository(ProviderEntity);

//   try {
//     const providerCreated = await queryRunner.save(provider);
//     console.log(
//       `provider_service: createprovider, providerCreated with id: ${providerCreated.id}`,
//     );
//   } catch (err: any) {
//     console.log('ProviderService: createProvider>>', err.message);
//     throw new InvoiceError(errorType.dataBase, 'Provider not creted');
//   }
// };

// export const getAllProviders = async (): Promise<NewProviderI[]> => {
//   try {
//     const query = AppDataSource.getRepository(ProviderEntity);
//     log;
//     const providers = await query.find();
//     return providers;
//   } catch (err: any) {
//     console.log('ProviderService: getAllProviders>>', err.message);
//     throw new InvoiceError(errorType.dataBase, 'Providers not found');
//   }
// };

// export const getProviderById = async (id: string): Promise<NewProviderI | null> => {
//   try {
//     const query = AppDataSource.getRepository(ProviderEntity);
//     const providerEntitie = await query.findOne({
//       where: { id },
//     });
//     return providerEntitie;
//   } catch (err: any) {
//     console.log('ProviderService: getProviderById>>', err.message);
//     throw new InvoiceError(errorType.dataBase, 'Providers not found');
//   }
// };

// export const updateProviderById = async (
//   provider: NewProviderI,
//   id: string,
// ): Promise<void> => {
//   try {
//     const query = AppDataSource.getRepository(ProviderEntity);
//     const userUpdated = await query
//       .createQueryBuilder()
//       .update(ProviderEntity)
//       .set(provider)
//       .where('id= :id', { id })
//       .execute();
//     if (userUpdated.affected === 0) {
//       throw new InvoiceError(errorType.notFound, 'provider not found');
//     }
//     console.log(`provider_service: updateproviderById,  providerUpdated with id: ${id}`);
//   } catch (err: any) {
//     console.log('ProviderService: updateProviderById>>', err.message);
//     throw new InvoiceError(errorType.dataBase, 'Providers not updated');
//   }
// };

// export const deactivateProviderById = async (
//   id: string,
//   updatedBy: string,
//   s,
// ): Promise<void> => {
//   try {
//     const query = AppDataSource.getRepository(ProviderEntity);
//     const userUpdated = await query
//       .createQueryBuilder()
//       .update(ProviderEntity)
//       .set({ isActive: false, updatedBy })
//       .where('id= :id', { id })
//       .execute();

//     if (userUpdated.affected === 0) {
//       throw new InvoiceError(errorType.notFound, 'provider not found');
//     }
//     console.log(
//       `user_service: deactivateproviderById,  providerDeactivate with id: ${id}`,
//     );
//   } catch (err: any) {
//     console.log('ProviderService: deactivateProviderById>>', err.message);
//     throw new InvoiceError(errorType.dataBase, 'Providers not deleted');
//   }
// };
