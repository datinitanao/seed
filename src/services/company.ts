// import CompanyEntity from '../db/entities/companyEntity';
// import AppDataSource from '../db/ormconfig';
// import { errorType } from '../utils/erroTypes';
// import InvoiceError from '../utils/invoiceError';

// // eslint-disable-next-line import/prefer-default-export
// export const findCompanyById = async (id: string): Promise<CompanyEntity> => {
//   const queryRunner = AppDataSource.getRepository(CompanyEntity);
//   try {
//     const company = await queryRunner.findOne({ where: { id }, relations: { taxValue: true } });
//     if (!company) {
//       throw new InvoiceError(errorType.dataBase, 'Company not found');
//     }
//     return company;
//   } catch (err: any) {
//     console.log('CompanyService: findCompanyById>>', err.message);
//     throw new InvoiceError(errorType.dataBase, 'Company not found');
//   }
// };
