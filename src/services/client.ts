import AppDataSource from '../db/ormconfig';

import InvoiceError from '../utils/invoiceError';
import { errorType } from '../utils/erroTypes';
import { NewClientI } from '../interfaces/client';
import ClientEntity from '../db/entities/clientEntity';
import { getKeyDbError } from '../utils/utils';

const createClient = async (client: NewClientI): Promise<void> => {
  const queryRunner = AppDataSource.getRepository(ClientEntity);

  try {
    const clientCreated = await queryRunner.save(client);
    console.log(
      `client_service: createClient, clientCreated with id: ${clientCreated.id}`,
    );
  } catch (err: any) {
    if (err.code === '23505') {
      throw new InvoiceError(errorType.dbUnique, getKeyDbError(err.detail));
    }
    throw new InvoiceError(errorType.dataBase, '', err);
  }
};

const getAllClients = async (): Promise<ClientEntity[]> => {
  try {
    const query = AppDataSource.getRepository(ClientEntity);
    return await query.find();
  } catch (err: any) {
    throw new InvoiceError(errorType.dataBase, '', err);
  }
};

export const getClientById = async (id: string): Promise<ClientEntity | null> => {
  try {
    const query = AppDataSource.getRepository(ClientEntity);
    const clientEntitie = await query.findOne({
      where: { id },
    });
    return clientEntitie;
  } catch (err:any) {
    throw new InvoiceError(errorType.dataBase, '', err);
  }
};

const updateClientById = async (
  client: NewClientI,
  id: string,
): Promise<void> => {
  try {
    const query = AppDataSource.getRepository(ClientEntity);
    const userUpdated = await query
      .createQueryBuilder()
      .update(ClientEntity)
      .set(client)
      .where('id= :id', { id })
      .execute();
    if (userUpdated.affected === 0) {
      throw new InvoiceError(errorType.notFound, 'client not found');
    }
    console.log(`client_service: updateClientById,  clientUpdated with id: ${id}`);
  } catch (err: any) {
    if (err.code === '23505') {
      throw new InvoiceError(errorType.dbUnique, getKeyDbError(err.detail));
    }
    throw new InvoiceError(errorType.dataBase, 'Client not updated');
  }
};

const deactivateClientById = async (
  id: string,
  updatedBy: string,
): Promise<void> => {
  try {
    const query = AppDataSource.getRepository(ClientEntity);
    const userUpdated = await query
      .createQueryBuilder()
      .update(ClientEntity)
      .set({ isActive: false, updatedBy })
      .where('id= :id', { id })
      .execute();

    if (userUpdated.affected === 0) {
      throw new InvoiceError(errorType.notFound, 'client not found');
    }
    console.log(
      `user_service: deactivateClientById,  ClientDeactivate with id: ${id}`,
    );
  } catch (err:any) {
    console.log('ClientService: deactivateClientById>>', err.message);
    throw new InvoiceError(errorType.dataBase, 'Client not deactivated');
  }
};
export default {
  createClient,
  getAllClients,
  getClientById,
  updateClientById,
  deactivateClientById,
};

// export const getClientByDni = async (dni: string): Promise<NewClientI> => {
//   try {
//     const query = AppDataSource.getRepository(ClientEntity);
//     const clientEntitie = await query.findOne({
//       where: { dni },
//     });
//     if (!clientEntitie) {
//       throw new InvoiceError(errorType.dataBase, 'Client not found');
//     }
//     return clientEntitie;
//   } catch (err:any) {
//     console.log('ClientService: getClientByDni>>', err.message);
//     throw new InvoiceError(errorType.dataBase, 'Client not found');
//   }
// };
