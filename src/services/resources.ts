import ResourceEntity from '../db/entities/resourcesEntity';
import AppDataSource from '../db/ormconfig';
import { errorType } from '../utils/erroTypes';
import InvoiceError from '../utils/invoiceError';
import { getResourceName } from '../utils/utils';

const getAllResourses = async (): Promise<ResourceEntity[]> => {
  const queryRunner = AppDataSource.getRepository(ResourceEntity);
  try {
    return await queryRunner.find();
  } catch (err) {
    throw new InvoiceError('db', '', err);
  }
};

const checkAccess = async (
  resource: string,
  action: string,
  access?: string[],
): Promise<boolean> => {
  try {
    if (resource !== undefined || resource !== null) {
      const query = AppDataSource.getRepository(ResourceEntity);
      const data = await query
        .createQueryBuilder('resources')
        .select()
        .where('resource= :resource', { resource: getResourceName(resource) })
        .getOne();
      const id = data?.actions?.find((act) => act.action === action.toLowerCase())?.id || '';
      return access ? access.includes(id) : false;
    }
    return false;
  } catch (err) {
    throw new InvoiceError(errorType.dataBase, '', err);
  }
};

export default {
  getAllResourses,
  checkAccess,
};
