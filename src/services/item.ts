// import CategoryEntity from '../db/entities/categoryEntity';
// import ItemEntity from '../db/entities/itemEntity';
// import AppDataSource from '../db/ormconfig';
// import { ItemEnum } from '../enums/ItemType';
// import { NewItemsI } from '../interfaces/items';
// import { errorType } from '../utils/erroTypes';
// import InvoiceError from '../utils/invoiceError';

// export const createItem = async (
//   items: NewItemsI[],
//   categoryId?: string,
// ): Promise<void> => {
//   try {
//     const query = AppDataSource.getRepository(ItemEntity);
//     const newItems = await query
//       .createQueryBuilder()
//       .insert()
//       .into(ItemEntity)
//       .values(items)
//       .execute();

//     if (categoryId) {
//       await query
//         .createQueryBuilder()
//         .relation(CategoryEntity, 'items')
//         .of(categoryId)
//         .add(newItems.identifiers);
//     }
//   } catch (err: any) {
//     console.log('ItemService: createItem>>', err.message);
//     if (err.code === '23505') {
//       throw new InvoiceError(errorType.dataBase, err.detail);
//     }
//     throw new InvoiceError(errorType.dataBase, 'Item not created');
//   }
// };

// export const updateItemById = async (
//   id: string,
//   item: NewItemsI,
// ): Promise<void> => {
//   try {
//     const query = AppDataSource.getRepository(ItemEntity);
//     const itemUpdated = await query
//       .createQueryBuilder()
//       .update(ItemEntity)
//       .set(item)
//       .where('id= :id', { id })
//       .execute();
//     if (itemUpdated.affected === 0) {
//       throw new InvoiceError(errorType.notFound, 'item not found');
//     }
//     console.log(`item_service: updateItemById,  itemUpdated with id: ${id}`);
//   } catch (err: any) {
//     console.log('ItemService: updateItemById>>', err.message);
//     throw new InvoiceError(errorType.dataBase, 'Items not updated');
//   }
// };

// export const getItemByCodeOrBarCode = async (
//   code: string,
// ): Promise<NewItemsI | null> => {
//   try {
//     const query = AppDataSource.getRepository(ItemEntity);
//     const item = await query
//       .createQueryBuilder()
//       .where('code= :code', { code })
//       .orWhere('"barCode"= :code', { code })
//       .getOne();

//     return item;
//   } catch (err: any) {
//     console.log('ItemService: getItemByCodeOrBarCode>>', err.message);
//     throw new InvoiceError(errorType.dataBase, 'Items not found');
//   }
// };

// export const getItems = async (): Promise<NewItemsI[]> => {
//   try {
//     const query = AppDataSource.getRepository(ItemEntity);
//     return await query
//       .createQueryBuilder('items')
//       .leftJoinAndSelect('items.categories', 'categories')
//       .getMany();
//   } catch (err: any) {
//     console.log('ItemService: getItemByCodeOrBarCode>>', err.message);
//     throw new InvoiceError(errorType.dataBase, 'Items not found');
//   }
// };

// export const getActiveItems = async (): Promise<NewItemsI[]> => {
//   try {
//     const query = AppDataSource.getRepository(ItemEntity);
//     return await query
//       .createQueryBuilder('items')
//       .where('items.isActive= :isActive', { isActive: true })
//       .getMany();
//   } catch (err: any) {
//     console.log('ItemService: getItemByCodeOrBarCode>>', err.message);
//     throw new InvoiceError(errorType.dataBase, 'Items not found');
//   }
// };

// export const getItemsByIds = async (ids: string[]): Promise<NewItemsI[]> => {
//   try {
//     const query = AppDataSource.getRepository(ItemEntity);
//     return await query
//       .createQueryBuilder('items')
//       .where('items.id IN (:...ids)', { ids })
//       .getMany();
//   } catch (err: any) {
//     console.log('ItemService: getItemsByIds>>', err.message);
//     throw new InvoiceError(errorType.dataBase, 'Items not found');
//   }
// };

// export const getItemById = async (id: string): Promise<NewItemsI | undefined | null> => {
//   try {
//     const query = AppDataSource.getRepository(ItemEntity);
//     const item = await query.createQueryBuilder().where('id= :id', { id }).getOne();
//     if (item?.type === ItemEnum.SERVICE) {
//       const service = { ...item, purchasePrice: item.pvps[0].value };
//       return service;
//     }
//     return item;
//   } catch (err: any) {
//     console.log('ItemService: getItemById>>', err.message);
//     throw new InvoiceError(errorType.dataBase, 'Items not found');
//   }
// };

// export const deactiveItemById = async (
//   id: string,
// ): Promise<void> => {
//   try {
//     const query = AppDataSource.getRepository(ItemEntity);
//     const itemUpdated = await query
//       .createQueryBuilder()
//       .update()
//       .set({ isActive: false })
//       .where('id= :id', { id })
//       .execute();
//     if (itemUpdated.affected === 0) {
//       throw new InvoiceError(errorType.notFound, 'item not found');
//     }
//   } catch (err: any) {
//     console.log('ItemService: deactiveItemById>>', err.message);
//     throw new InvoiceError(errorType.dataBase, 'Items not deactivated');
//   }
// };

// export const setItemToCategories = async (itemId: string, categories: string[]): Promise<void> => {
//   try {
//     const query = AppDataSource.getRepository(ItemEntity);
//     await query
//       .createQueryBuilder()
//       .relation(ItemEntity, 'categories')
//       .of(itemId)
//       .add(categories);
//   } catch (err: any) {
//     console.log('ItemService: setItemToCategories>>', err.message);
//     throw new InvoiceError(errorType.dataBase, 'Items not set to category');
//   }
// };
