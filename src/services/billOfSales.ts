// import AppDataSource from '../db/ormconfig';

// import InvoiceError from '../utils/invoiceError';
// import BillOfSalesEntity from '../db/entities/BillOfSaleEntity';
// import { NewBillI } from '../interfaces/bill';
// import { errorType } from '../utils/erroTypes';
// import { SriStatus } from '../enums/sriEnums';

// export const createBillOfSales = async (bill: NewBillI): Promise<void> => {
//   const queryRunner = AppDataSource.getRepository(BillOfSalesEntity);

//   try {
//     const clientCreated = await queryRunner.save(bill);
//     console.log(
//       `billOfSalesServices: createBillOfSales, bill created with id: ${clientCreated.id}`,
//     );
//   } catch (err) {
//     throw new InvoiceError('db', '', err);
//   }
// };

// export const getAllBillOfSales = async (): Promise<NewBillI[]> => {
//   try {
//     const query = AppDataSource.getRepository(BillOfSalesEntity);
//     const bill = await query.find();
//     return bill;
//   } catch (err) {
//     throw new InvoiceError('db', '', err);
//   }
// };

// export const getBillOfSalesById = async (id: string): Promise<NewBillI | null> => {
//   try {
//     const query = AppDataSource.getRepository(BillOfSalesEntity);
//     return await query.findOne({
//       where: { id },
//     });
//   } catch (err) {
//     throw new InvoiceError('db', '', err);
//   }
// };

// export const getBillOfSalesByAccesskey = async (accessKey: string): Promise<NewBillI> => {
//   try {
//     const query = AppDataSource.getRepository(BillOfSalesEntity);
//     const bill = await query
//       .createQueryBuilder('bill')
//       .select()
//       .leftJoinAndSelect('bill.details', 'details')
//       .where('bill.accessKey = :accessKey', { accessKey })
//       .getOne();
//     if (!bill) {
//       throw new InvoiceError(errorType.dataBase, 'Bill not Found');
//     }
//     return bill;
//   } catch (err: any) {
//     console.log('BillOfSales: getBillOfSalesByAccesskey>>', err.message);
//     throw new InvoiceError(errorType.dataBase, 'Bill not Found');
//   }
// };

// export const updateBillOfSalesByAccesskey = async (accessKey: string, status: SriStatus) => {
//   try {
//     const query = AppDataSource.getRepository(BillOfSalesEntity);
//     const bill = await query
//       .createQueryBuilder()
//       .update(BillOfSalesEntity)
//       .set({ status })
//       .where('accessKey = :accessKey', { accessKey })
//       .execute();
//     if (!bill) {
//       throw new InvoiceError(errorType.dataBase, 'Bill not Found');
//     }
//     console.log(`updateBillOfSalesByAccesskey: was updated by accessKey:${accessKey} and status: ${status}`);
//   } catch (err: any) {
//     console.log('BillOfSales: getBillOfSalesByAccesskey>>', err.message);
//     throw new InvoiceError(errorType.dataBase, 'Bill not Found');
//   }
// };

// export const getBillOfSalesByUserAndDate = async (
//   dni: string,
//   date: Date,
// ): Promise<NewBillI[] | null> => {
//   try {
//     const query = AppDataSource.getRepository(BillOfSalesEntity);
//     return await query
//       .createQueryBuilder('bill')
//       .select()
//       .where('dni= :dni', { dni })
//       .andWhere('date= :date', { date })
//       .getMany();
//   } catch (err) {
//     throw new InvoiceError('db', '', err);
//   }
// };

// export const getBillOfSalesBetweenDates = async (
//   startDate: Date,
//   endDate: Date,
// ): Promise<NewBillI[] | null> => {
//   try {
//     const query = AppDataSource.getRepository(BillOfSalesEntity);
//     return await query
//       .createQueryBuilder('bill')
//       .select()
//       .where('bill.date > :startDate', { startDate })
//       .andWhere('bill.date < :endDate', { endDate })
//       .getMany();
//   } catch (err) {
//     throw new InvoiceError('db', '', err);
//   }
// };

// export const cancelBillOfSales = async (
//   id: string,
//   updatedBy: string,
// ): Promise<void> => {
//   try {
//     const query = AppDataSource.getRepository(BillOfSalesEntity);
//     const userUpdated = await query
//       .createQueryBuilder()
//       .update(BillOfSalesEntity)
//       .set({ isActive: false, updatedBy })
//       .where('id= :id', { id })
//       .execute();
//     if (userUpdated.affected === 0) {
//       throw new InvoiceError(errorType.notFound, 'BillOfSales not found');
//     }
//     console.log(
//       `BillOfSales_service: updateBillOfSalesById,  BillOfSalesUpdated with id: ${id}`,
//     );
//   } catch (err) {
//     throw new InvoiceError('db', '', err);
//   }
// };
