import { round } from 'mathjs';
import { Detail, TotalBill } from '../interfaces/bill';
import { NewItemsI } from '../interfaces/items';
import { CommonI, NewUserI } from '../interfaces/user';
// import { getItemsByIds } from '../services/item';

export const addCreatedAndUpdatedByToSchema = (
  schemas: CommonI[],
  createdBy: string,
  updatedBy: string,
  companyId: string,
) => schemas.map((schema: CommonI) => {
  const newAddress = {
    ...schema,
    createdBy,
    updatedBy,
    companyId,
  };
  return newAddress;
});

// export const createBillOfSalesDetails = async (
//   details: any,
//   taxValue: number,
// ): Promise<TotalBill> => {
//   const itemIds = details.map((detail: any) => detail.id);
//   const items = await getItemsByIds(itemIds);
//   let totalIva = 0;
//   let totalZero = 0;
//   let tax = 0;
//   const newDetails = items.map((item: NewItemsI) => {
//     const detail = details.find((detail: any) => detail.id === item.id);
//     const price = item.pvps.find((pvp: any) => pvp.id === detail.pvp);
//     const unitPrice = price ? price.value : 0;

//     const total = detail.amount * unitPrice;

//     const iva = item.hasIva ? (taxValue / 100) * total : 0;
//     const newitem: Detail = {
//       code: item.code ?? '',
//       amount: detail.amount,
//       detail: item.name,
//       unitPrice,
//       total,
//       tax: round(iva, 2),
//       discount: 0,
//     };
//     tax = round(tax + iva, 2);
//     if (item.hasIva) {
//       totalIva += total;
//     } else {
//       totalZero += total;
//     }
//     return newitem;
//   });

//   return {
//     details: newDetails,
//     subTotalTax: totalIva,
//     subTotalZero: totalZero,
//     totaltax: tax,
//     totalDiscount: 0,
//   };
// };

// export const sleep = async (milliseconds: number): Promise<void> => {
//   await new Promise((resolve) => setTimeout(resolve, milliseconds));
// };

export const getKeyDbError = (detail:string): string | undefined => {
  const match = detail.match(/\((\w+)\)/);
  if (match) {
    return `${match[1]} already exists`;
  }
  return undefined;
};

export const getResourceName = (detail:string): string | undefined => {
  const match = detail.match(/^\/([a-zA-Z]+)(\/.*)?$/);
  if (match) {
    return `${match[1]}`;
  }
  return undefined;
};

export const passwordCreation = (user: NewUserI): string => `${Array.from(user.name)[0]}${user.dni}${Array.from(user.lastName)[0]}`;
