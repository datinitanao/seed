export const errorType = {
  authentication: 'oauth',
  Forbidden: 'forbidden',
  validate: 'validate',
  notFound: 'notFound',
  dataBase: 'db',
  dbUnique: 'unique',
  sri: 'sri',
};
