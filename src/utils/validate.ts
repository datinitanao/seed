import InvoiceError from './invoiceError';

const validator = async (schame: any, data: any) => {
  try {
    await schame.validateAsync(data, { abortEarly: false });
  } catch (err: any) {
    throw new InvoiceError('validate', err.details[0].message, err);
  }
};

export default validator;
