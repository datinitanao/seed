import { Environment, TypeOfEmition } from '../enums/sriEnums';

export const elevenModule = (accessKey: string) => {
  const accessKeyRevert = accessKey.split('').reverse().map((elem) => parseInt(elem, 10));
  let count = 2;
  const sum = accessKeyRevert.reduce((acc: any, curValue: number) => {
    acc += (curValue * count);
    count = count === 7 ? 2 : count + 1;
    return acc;
  }, 0);
  const mod = sum % 11;
  const result = 11 - mod;
  return result === 11 ? 0 : result === 10 ? 1 : result;
};

export const accessKeyGenerator = (
  transactionDate: string,
  documentType: string,
  ruc: string,
  environment: Environment,
  branchOffice: string,
  boxCode: string,
  documentNumber: string,
  ownerCode: string,
  typeOfEmition: TypeOfEmition,
): string => {
  const accessKey = `${transactionDate}${documentType}${ruc}${environment}${branchOffice}${boxCode}${documentNumber}${ownerCode}${typeOfEmition}`;
  const newAccessKey = elevenModule(accessKey);
  return `${accessKey}${newAccessKey}`;
};

const padTo2Digits = (num: number) => num.toString().padStart(2, '0');

export const getDateDDMMAAAA = (date: string): string => {
  const newDate = new Date(`${date} GMT-0500`);
  return [
    padTo2Digits(newDate.getDate()),
    padTo2Digits(newDate.getMonth() + 1),
    newDate.getFullYear(),
  ].join('');
};

export const getDateFormat = (date: Date): string => {
  const newDate = new Date(`${date} GMT-0500`);
  return [
    padTo2Digits(newDate.getDate()),
    padTo2Digits(newDate.getMonth() + 1),
    date.getFullYear(),
  ].join('/');
};
