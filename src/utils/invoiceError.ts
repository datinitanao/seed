export default class InvoiceError extends Error {
  type: string;

  message: string;

  err?: number;

  constructor(type: string, message = 'Internal Server Error', err?: any) {
    super(err);
    this.type = type;
    this.message = message;
    this.err = err;
  }
}
