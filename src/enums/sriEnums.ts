export enum DocumentName {
  'FACTURA' = '01',
  'LIQUIDACION_DE_COMPRA_DE_BIENES_Y_PRESTACION_DE_SERVICIOS' = '03',
  'NOTA_DE_CREDITO' = '04',
  'NOTA_DE_DEBITO' = '05',
  'GUIA_DE_REMISION' = '06',
  'COMPROBANTE_DE_RETENCION' = '07',
}

export enum Environment {
  'PRUEBAS' = 1,
  'PRODUCCION' = 2,
}

export enum TypeOfEmition {
  'EMISION_NORMAL' = 1,
}

export enum Flag {
  SI = 'SI',
  NO = 'NO',
}

export enum TypeOfIdentificationBuyer {
  RUC = '04',
  CEDULA = '05',
  PASAPORTE = '06',
  VENTA_A_CONSUMIDOR_FINAL = '07',
  DENTIFICACION_DEL_EXTERIOR = '08',
}

export enum TypeOfTaxs {
  IVA = 2,
  ICE = 3,
  IRBPNR = 5,
}

export enum CodeOfTax {
  'ZERO' = 0,
  'TWELVE' = 2,
  'FOURTEEN' = 3,
  'NO_OBJECT_OF_TAX' = 6,
  'NOT_TAX' = 7,
}

export enum MethodOfPayment {
  SIN_UTILIZACION_DEL_SISTEMA_FINANCIERO = '01',
  COMPENSACION_DE_DEUDAS = '15',
  TARJETA_DE_DEBITO = '16',
  DINERO_ELECTRONICO = '17',
  TARJETA_PREPAGO = '18',
  TARJETA_DE_CREDITO = '19',
  OTROS_CON_UTILIZACION_DEL_SISTEMA_FINANCIERO = '20',
  ENDOSO_DE_TITULOS = '21',
}

export enum SriStatus {
  NO_ENVIADA = 'NO_ENVIADA',
  DEVUELTA = 'DEVUELTA',
  RECIBIDA = 'RECIBIDA',
  AUTORIZADO = 'AUTORIZADO',
  NO_AUTORIZADO = 'NO AUTORIZADO',
}
