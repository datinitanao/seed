import Joi from 'joi';
import { AddressSchema } from './addressSchema';
import { PersonSchema } from './personSchema';
import { PhoneSchema } from './phoneSchema';

export const UserSchema = PersonSchema.keys({
  phones: Joi.array().items(PhoneSchema).min(1).required(),
  addresses: Joi.array().items(AddressSchema).min(1).required(),
  resources: Joi.array().items(Joi.string().uuid()),
}).required()
  .label('User');
