import Joi from 'joi';

// eslint-disable-next-line import/prefer-default-export
export const CommonSchema = Joi.object().keys({
  type: Joi.string().required(),
  value: Joi.string().required(),
  description: Joi.string().allow(null),
});
