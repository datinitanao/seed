import Joi from 'joi';

export const PhoneSchema = Joi.object({
  type: Joi.string().required(),
  value: Joi.string().required(),
})
  .required()
  .label('Phone');
