import Joi from 'joi';
import { PersonSchema } from './personSchema';

// eslint-disable-next-line import/prefer-default-export
export const ClientSchema = PersonSchema.keys({
  phone: Joi.string().required(),
  address: Joi.string().required(),
  createdBy: Joi.string().guid().allow(null),
  createdAt: Joi.string().allow(null),
  updatedAt: Joi.string().allow(null),
})
  .required()
  .label('Client')
  .options({
    abortEarly: false,
  });
