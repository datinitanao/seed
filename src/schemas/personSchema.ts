import Joi from 'joi';

// eslint-disable-next-line import/prefer-default-export
export const PersonSchema = Joi.object().keys({
  name: Joi.string().required(),
  lastName: Joi.string().required(),
  email: Joi.string()
    .email({ minDomainSegments: 2, tlds: { allow: ['com'] } })
    .required(),
  dni: Joi.string().required(),
  createdBy: Joi.string().guid().default(''),
  updatedBy: Joi.string().guid().default(''),
  isActive: Joi.boolean().default(true),
});
