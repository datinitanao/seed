import Joi from 'joi';

// eslint-disable-next-line import/prefer-default-export
export const CredentialsSchema = Joi.object({
  csrfToken: Joi.string().required(),
  username: Joi.string().required(),
  password: Joi.string().required(),
})
  .required()
  .label('Credentials');
