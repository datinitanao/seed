import Joi from 'joi';
import { MethodOfPayment, TypeOfIdentificationBuyer } from '../enums/sriEnums';

// eslint-disable-next-line import/prefer-default-export
export const BillSchema = Joi.object().keys({
  dni: Joi.string().required(),
  date: Joi.date().iso().required(),
  createdBy: Joi.string().guid().required(),
  updatedBy: Joi.string().guid().required(),
  isActive: Joi.boolean().default(true),
  companyId: Joi.string().guid().required(),
  boxId: Joi.string().guid().required(),
  branchOfficeId: Joi.string().guid().required(),
  identificationType: Joi.string().valid(...Object.values(TypeOfIdentificationBuyer)).required(),
  methodOfPayment: Joi.string().valid(...Object.values(MethodOfPayment)).required(),
  details: Joi.array().items(Joi.object({
    id: Joi.string().guid().required(),
    amount: Joi.number().required(),
    pvp: Joi.number().required(),
  })).min(1).required(),
}).required()
  .label('Bill');
