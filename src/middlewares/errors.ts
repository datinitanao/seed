import { Response } from 'express';
import httpStatus from 'http-status-codes';
import { errorType } from '../utils/erroTypes';

const handleErrors = (
  err: any,
  res: Response,
): void => {
  let status: number = httpStatus.INTERNAL_SERVER_ERROR;
  let msg: string;
  console.log(err);
  switch (err.type) {
    case errorType.validate:
      msg = err.message;
      status = httpStatus.BAD_REQUEST;
      break;
    case errorType.authentication:
      msg = err.message;
      status = httpStatus.UNAUTHORIZED;
      break;
    case errorType.Forbidden:
      msg = err.message;
      status = httpStatus.FORBIDDEN;
      break;
    case errorType.notFound:
      msg = err.message;
      status = httpStatus.NOT_FOUND;
      break;
    case errorType.dataBase:
      msg = err.message;
      status = httpStatus.SERVICE_UNAVAILABLE;
      break;
    case errorType.dbUnique:
      msg = err.message;
      status = httpStatus.BAD_REQUEST;
      break;
    default:
      msg = 'server internal error';
      break;
  }
  res.status(status).json({ msg });
};

export default { handleErrors };
