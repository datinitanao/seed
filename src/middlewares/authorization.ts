import { Response, NextFunction } from 'express';
import { CustomRequest } from '../interfaces/customRequest';
import ResourcesService from '../services/resources';
import { errorType } from '../utils/erroTypes';
import InvoiceError from '../utils/invoiceError';
import Errors from './errors';

const checkAccess = async (
  req: CustomRequest,
  res: Response,
  next: NextFunction,
):
Promise<void> => {
  try {
    const {
      method, route: { path },
      user,
    } = req;
    const hasAccess = await ResourcesService.checkAccess(path, method, user?.resources);
    if (!user?.owner && !hasAccess) {
      throw new InvoiceError(errorType.Forbidden, 'Forbidden');
    }
    next();
  } catch (error) {
    Errors.handleErrors(error, res);
  }
};

export default checkAccess;
