import { Response, NextFunction } from 'express';
import { decodeToken } from '../services/auth';
import UserService from '../services/user';
import Errors from './errors';

const checkToken = async (req: any, res: Response, next: NextFunction): Promise<void> => {
  try {
    const {
      method,
      headers: { token },
    } = req;

    const { userId }: any = await decodeToken(token);
    const user = await UserService.getUserById(userId);

    req.user = user;
    if (method === 'POST') {
      req.body.createdBy = userId;
      req.body.updatedBy = userId;
    }
    if (method === 'PUT' || method === 'DELETE') {
      req.body.updatedBy = userId;
    }
    next();
  } catch (error) {
    Errors.handleErrors(error, res);
  }
};

export default checkToken;
