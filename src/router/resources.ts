import { Router } from 'express';
import ResourcesController from '../controllers/resources';
import checkToken from '../middlewares/authentication';

const router = Router();

router.get('/resources', [checkToken], ResourcesController.getAllResource);

export default router;
