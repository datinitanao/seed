import { Router } from 'express';
import UserController from '../controllers/user';
import checkToken from '../middlewares/authentication';
import checkAccess from '../middlewares/authorization';

const router = Router();

router.post('/user', [checkToken, checkAccess], UserController.createUser);
router.get('/user', [checkToken, checkAccess], UserController.getAllUsers);
router.get('/user/access', [checkToken], UserController.getAccess);
router.get('/user/:id', [checkToken, checkAccess], UserController.getUserById);
router.put('/user/:id', [checkToken, checkAccess], UserController.updateUser);

export default router;
