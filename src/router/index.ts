import { Router } from 'express';
import Auth from './auth';
import User from './user';
// import Phone from './phone';
import Client from './client';
import Resources from './resources';
// import Provider from './provider';
// import Item from './item';
// import Category from './category';
// import Sales from './billOfSales';
// import Files from './files';

const router = Router();

router.use(Auth);
router.use(Resources);
router.use(User);
// router.use(Phone);
router.use(Client);
// router.use(Provider);
// router.use(Item);
// router.use(Category);
// router.use(Sales);
// router.use(Files);

export default router;
