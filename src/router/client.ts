import { Router } from 'express';
import ClientController from '../controllers/client';
import checkToken from '../middlewares/authentication';
import checkAccess from '../middlewares/authorization';

const router = Router();

router.post('/client', [checkToken, checkAccess], ClientController.createClient);
router.get('/client', [checkToken, checkAccess], ClientController.getAllClients);
router.get('/client/:id', [checkToken, checkAccess], ClientController.getClientById);
router.put('/client/:id', [checkToken, checkAccess], ClientController.updateClientById);
router.delete('/client/:id', [checkToken, checkAccess], ClientController.deleteClientById);

export default router;
